import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

interface Pokemon {
  id: number;
  name: string;
}

@Component({
  selector: 'app-toto',
  templateUrl: './toto.component.html',
  styleUrls: ['./toto.component.scss']
})
export class TotoComponent {

  private http: HttpClient;

  public form: FormGroup = new FormGroup({
    pokemon: new FormControl(null)
  });

  public pokemons: Pokemon[] = [
    {id: 1, name: 'Salamèche'},
    {id: 2, name: 'Bulbizare'},
    {id: 3, name: 'Carapuce'}
  ];

  constructor(http: HttpClient) {
    this.http = http;
  }

  public onSummit() {
    this.http.post('/', this.form.value).subscribe();
  }

}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomepageComponent } from './homepage/homepage.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { MyListComponent } from './my-list/my-list.component';
import { GameDetailComponent } from './game-detail/game-detail.component';
import { ProfilComponent } from './profil/profil.component';
import {SearchComponent} from './search/search.component';

const routes: Routes = [
  // {path: '', component: LoginComponent},
  {path: '', component: LoginComponent},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'home', component: HomepageComponent},
  {path: 'myList', component: MyListComponent},
  {path: 'game/:id', component: GameDetailComponent},
  {path: 'profil', component: ProfilComponent},
  {path: 'search', component: SearchComponent},
  // {path: 'detail/:id', component: DetailComponent},
  // {path: 'locations', component: LocationComponent},
  // {path: 'location/:id', component: LocationDetailComponent},
  // {path: 'area/:id', component: AreaDetailComponent},
  {path: '**', component: PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

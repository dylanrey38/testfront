import {Component, Inject, OnInit} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import {DialogComponent} from '../dialog/dialog.component';
import {DialogMdpComponent} from '../dialog-mdp/dialog-mdp.component';
import {FileUploadComponent} from '../file-upload/file-upload.component';
import {isNaN} from 'lodash-es';
import {CookieService} from 'ngx-cookie-service';
import {HttpClient} from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.scss']
})
export class ProfilComponent implements OnInit {
  name: string | undefined;
  newName: string | undefined;
  newPassword: string | undefined;

  userId = -1;
  public username = '';
  public email = '';
  public urlPP = 'http://127.0.0.1:8080/media/';

  constructor(public dialog: MatDialog, private cookieService: CookieService, private http: HttpClient) {}

  ngOnInit(): void {
    window.scrollTo(0, 0);
    this.userId = parseInt(this.cookieService.get('UserID'), 10);

    this.http.get<any>('http://127.0.0.1:8080/api/users/getprofileinfo/' + this.userId)
      .subscribe({
        next: data => {
          this.username = data.username;
          this.email = data.mail;
          this.urlPP += data.pictureurl;
        },
        error: error => {
          this.cookieService.set('UserID', '', { expires: 30, sameSite: 'Lax' });
        }
      });
  }


  /* Modifier la photo de profil */
  openDialogProfil(): void {
    const dialogRef = this.dialog.open(FileUploadComponent, {
      width: '300px',
      data: {name: this.name, newName: this.newName}
    });

    dialogRef.afterClosed().subscribe(result => {
      this.newName = result;
    });
  }

  /* Modifier le nom d'utilisateur */
  openDialog(): void {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '250px',
      data: {name: this.name, newName: this.newName}
    });

    dialogRef.afterClosed().subscribe(result => {
      this.newName = result;
    });
  }

  /* Modifier mot de passe */
  openDialogMdp(): void{
    const dialogRef = this.dialog.open(DialogMdpComponent, {
      width: '400px',
      // data: {newPassord: this.newPassword}
    });
  }

}

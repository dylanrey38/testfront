import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  hide = true;
  userId = -1;
  stayConnected = false;

  userForm!: FormGroup;
  submitted = false;
  constructor(private fb: FormBuilder,
              private router: Router,
              private http: HttpClient,
              private cookieService: CookieService,
              private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.userForm = this.fb.group({
      txtName: ['', Validators.required],
      txtPassword: ['', [Validators.required, Validators.minLength(6)]]
    });

    if (this.cookieService.get('UserID') !== '') {
      this.router.navigateByUrl('home');
    }
  }

  get getForm(): { [p: string]: AbstractControl } {
    return this.userForm.controls;
  }

  onSubmit(): void {
    this.submitted = true;
    if (!this.userForm.valid) {
      return;
    }

    this.http.post<any>('http://127.0.0.1:8080/api/users/connect',
      { username: this.userForm.value.txtName, password: this.userForm.value.txtPassword })
      .subscribe({
      next: data => {
        this.userId = data.id;
        if (this.stayConnected) {
          this.cookieService.set('UserID', String(this.userId), { expires: 30, sameSite: 'Lax' });
        }
        else {
          this.cookieService.set('UserID', String(this.userId));
        }
        this.router.navigateByUrl('home');
      },
      error: error => {
        // this.errorMessage = error.message;
        this.snackBar.open('L\'utilisateur ou le mot de passe est incorrect', 'Ok!', {
          duration: 3000
        });
      }
    });
  }

  onReset(): void {
    this.submitted = false;
    this.userForm.reset();
  }

  checkStayConnectValue(event: any): void {
    this.stayConnected = event.checked;
  }
}

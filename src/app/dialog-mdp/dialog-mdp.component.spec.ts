import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogMdpComponent } from './dialog-mdp.component';

describe('DialogMdpComponent', () => {
  let component: DialogMdpComponent;
  let fixture: ComponentFixture<DialogMdpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialogMdpComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogMdpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

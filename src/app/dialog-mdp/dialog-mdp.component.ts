import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FormBuilder, Validators} from '@angular/forms';
import {MustMatch} from '../utils/must-match.validator';
import { HttpClient } from '@angular/common/http';
import {CookieService} from 'ngx-cookie-service';

export interface DialogData {
  nowPassword: string;
  password: string;
  newPassword: string;
}

@Component({
  selector: 'app-dialog-mdp',
  templateUrl: './dialog-mdp.component.html',
  styleUrls: ['./dialog-mdp.component.scss']
})
export class DialogMdpComponent implements OnInit {
  hide = true;
  submitted = false;
  userId = -1;

  formGroup = this._formBuilder.group({
    nowPassword: ['', Validators.required], /* A récupérer de la base de données*/
    password: ['', [Validators.required, Validators.minLength(6)]],
    confirmPassword: ['', Validators.required],
  }, {
    validator: MustMatch('password', 'confirmPassword')
  });

  // tslint:disable-next-line:variable-name
  constructor(private _formBuilder: FormBuilder, public dialogRef: MatDialogRef<DialogMdpComponent>,
              @Inject(MAT_DIALOG_DATA) public data: DialogData, private http: HttpClient, private cookieService: CookieService) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
  // tslint:disable-next-line:typedef
  onSubmit() {
    this.submitted = true;
    if (!this.formGroup.valid) {
      return;
    }

    let form = JSON.stringify(this.formGroup.value);
    form = form.substr(0, form.length - 1);
    form += ',"id":"' + this.userId + '"}';

    this.http.post('http://127.0.0.1:8080/api/users/change_password', form).subscribe({
      next: data => {
        window.location.reload();
      },
        error: error => {
        alert('Erreur lors du changement de mot de passe');
      }
    });
  }
  // tslint:disable-next-line:typedef
  get getConnexion(){ return this.formGroup.controls; }

  ngOnInit(): void {
    this.userId = parseInt(this.cookieService.get('UserID'), 10);
  }
}

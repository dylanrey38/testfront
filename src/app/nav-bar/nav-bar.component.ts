import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import {isNaN} from 'lodash-es';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {

  public username = '';
  public url_pp = 'http://127.0.0.1:8080/media/';

  userId = -1;
  constructor(private router: Router,
              private cookieService: CookieService,
              private http: HttpClient,
              ) { }

  ngOnInit(): void {
    this.userId = parseInt(this.cookieService.get('UserID'), 10);
    if (isNaN(this.userId)) {
      this.router.navigateByUrl('');
    }

    this.http.get<any>('http://127.0.0.1:8080/api/users/getprofileinfo/' + this.userId)
      .subscribe({
        next: data => {
          this.username = data.username;
          this.url_pp += data.pictureurl;
        },
        error: error => {
          this.cookieService.set('UserID', '', { expires: 30, sameSite: 'Lax' });
          this.router.navigateByUrl('login');
        }
      });
  }

  account(): void {
    this.router.navigateByUrl('/profil');
  }

  disconnect(): void {
    this.cookieService.delete('UserID');
    this.router.navigateByUrl('');
  }
}

import {Component, OnInit} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { MustMatch } from '../utils/must-match.validator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import {parseInt} from 'lodash-es';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
  providers: [{
    provide: STEPPER_GLOBAL_OPTIONS, useValue: {displayDefaultIndicatorType: false}
  }]
})
export class RegisterComponent implements OnInit {
  submitted = false;
  submittedFinal = false;
  hide = true;
  term = false;
  isDisabled = true;

  selectedCat = '';

  /****Checkbox*********/
  categories = [
    { id: 11, name: 'Mr. Nice' }
  ];

  firstFormGroup = this.formBuilder.group({
    name: ['', Validators.required],
    firstname: ['', Validators.required],
    email: ['', [Validators.required, Validators.email]],
  });
  secondFormGroup = this.formBuilder.group({
    username: ['', Validators.required],
    password: ['', [Validators.required, Validators.minLength(6)]],
    confirmPassword: ['', Validators.required],
  }, {
    validator: MustMatch('password', 'confirmPassword')
  });

  thirdFormGroup = this.formBuilder.group({
    categories: ['', Validators.required]
  });

  fourthFormGroup = this.formBuilder.group({
    acceptTerms: [false, Validators.requiredTrue]
  });


  constructor(private formBuilder: FormBuilder,
              private http: HttpClient,
              private router: Router,
              private snackBar: MatSnackBar,
              private cookieService: CookieService) {
    // this.stepperOrientation = breakpointObserver.observe('(min-width: 800px)')
      // .pipe(map(({matches}) => matches ? 'horizontal' : 'vertical'));
  }

  ngOnInit(): void {
    this.http.get<any>('http://127.0.0.1:8080/api/games/getAllCats')
      .subscribe({
        next: data => {
          this.categories = [];
          console.log(data);
          for (const cat of data) {
            this.categories.push({ id: cat[0].id, name: cat[1].name});
          }
        }
      });
  }

  onSubmitId(): void {
    this.submitted = true;
    if (!this.firstFormGroup.valid) {
      return;
    }
  }

  onSubmit(): void {
    this.submitted = true;
    this.submittedFinal = true;
    console.log(this.term);
    if (!this.firstFormGroup.valid || !this.secondFormGroup.valid || !this.term) {
      return;
    }

    // alert(JSON.stringify(this.thirdFormGroup.value.name));

    this.http.post<any>('http://127.0.0.1:8080/api/users/register',
      {
        name: this.firstFormGroup.value.name,
        firstname: this.firstFormGroup.value.firstname,
        mail: this.firstFormGroup.value.email,
        username: this.secondFormGroup.value.username,
        password: this.secondFormGroup.value.password,
        categories: this.thirdFormGroup.value.categories.checked,
      })
      .subscribe({
        next: data => {
          if (data.id === -3) {
            this.snackBar.open('Erreur du mot de passe', 'Je modifie!', {
              duration: 3000
            });
          }
          else if (data.id === -2 ) {
            this.snackBar.open('Le nom d\'utilisateur existe déjà', 'Je modifie!', {
              duration: 3000
            });
          }
          else if (data.id === -1 ) {
            this.snackBar.open('Informations incorrectes', 'Je modifie!', {
              duration: 3000
            });
          }
          else {
            this.cookieService.set('UserID', String(data.id));
            console.log('OUI');
            this.http.post<any>('http://127.0.0.1:8080/api/users/firstUserPreference/' + data.id,
              '{"genres":"' + this.selectedCat + '"}')
              .subscribe();
            setTimeout(() => {
              this.router.navigateByUrl('home');
            }, 2000);
          }
        },
        error: error => {
          // this.errorMessage = error.message;
          this.snackBar.open('Une erreur est survenue', 'Ok!', {
            duration: 3000
          });
        }
      });
  }

  get getId() { return this.firstFormGroup.controls; }
  get getConnexion() { return this.secondFormGroup.controls; }
  get getCategories() { return this.thirdFormGroup.controls; }
  get getCondition() { return this.fourthFormGroup.controls; }

  onReset(): void {
    this.submitted = false;
    this.firstFormGroup.reset();
    this.secondFormGroup.reset();
    this.fourthFormGroup.reset();
    this.router.navigateByUrl('');
  }

  termActive(event: any): void {
    this.term = event.checked;
  }

  // tslint:disable-next-line:typedef
  doAction(){
    const max = 6;
    let z = 0;
    let name = '';
    const categ = document.getElementById('categ');
    const checkboxes = document.getElementsByClassName('case');
    for (let i = 0; i < checkboxes.length; i++) {
      // @ts-ignore
      if (checkboxes.item(i).checked === true) {
        z++;
        // @ts-ignore
        name += checkboxes.item(i).value + ',';
      }
    }
    if (z === 3){
      // @ts-ignore
      this.isDisabled = false;
      this.selectedCat = name.substr(0, name.length - 1);
      // alert(this.selectedCat);
    }else {
      // @ts-ignore
      this.isDisabled = true;
    }
  }
}



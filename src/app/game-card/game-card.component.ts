import { Component, Input, OnInit } from '@angular/core';
import { Pokemon } from '../commons/models/pokemon';

@Component({
  selector: 'app-game-card',
  templateUrl: './game-card.component.html',
  styleUrls: ['./game-card.component.scss']
})
export class GameCardComponent implements OnInit {

  @Input()
  public displayDetailButton = true;

  @Input('game')
  public game = <any>[];

  public name = '';
  public id = -1;
  public cover = '';

  constructor() { }

  ngOnInit(): void {
    this.name = this.game[0].name;
    this.id = this.game[0].id;
    this.cover = this.game[2].cover_url;
  }
}

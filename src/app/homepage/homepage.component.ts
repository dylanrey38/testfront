import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})

export class HomepageComponent implements OnInit {

  public forYou = 'Selected for you';
  public otherPlayer = 'Other players also like';
  public new = 'New released games';
  public bestMark = 'Best rated games';

  constructor() { }

  ngOnInit(): void {
    window.scrollTo(0, 0);
  }
}

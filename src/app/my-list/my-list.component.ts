import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Pokemon } from '../commons/models/pokemon';
import { PokemonService } from '../pokemon.service';
import { SearchPokemonService } from '../search-pokemon.service';
import {HttpClient} from '@angular/common/http';
import {CookieService} from 'ngx-cookie-service';

@Component({
  selector: 'app-my-list',
  templateUrl: './my-list.component.html',
  styleUrls: ['./my-list.component.scss']
})
export class MyListComponent implements OnInit {

  public games: any[] = [];
  public count = 0;
  public page = 0;
  public nextSearchIndex = 0;

  private searchWordsSubscription?: Subscription;
  private watchIndexedPokemonsSubscription?: Subscription;

  constructor(
    private pokemonService: PokemonService,
    private searchPokemonService: SearchPokemonService,
    private http: HttpClient,
    private cookieService: CookieService
  ) {}

  userId = -1;

  ngOnInit(): void {
    this.userId = parseInt(this.cookieService.get('UserID'), 10);

    this.games = [];
    this.http.get<any>(
      'http://127.0.0.1:8080/api/games/gameAddToList/' + this.userId)
      .subscribe({
        next: data => {
          this.games = [];
          for (const game of data) {
            this.games.push(game);
          }
        }
      });
  }


  /*private loadMoreResults(): void {
    if (this.searchWords.length === 0) {
      this.pokemonService.getPokemons(this.page).then((page) => {
        this.pokemons.push(...page.content);
        this.count = page.count;
      });
    } else {
      if (this.nextSearchIndex !== -1) {
        const [res, nextSearchIndex] = this.searchPokemonService.searchPokemon(this.searchWords, 20, this.nextSearchIndex);
        this.nextSearchIndex = nextSearchIndex;
        this.pokemons?.push(...res);
        if (nextSearchIndex === -1) {
          this.watchIndexedPokemonsSubscription = this.searchPokemonService.watchIndexedPokemeon().subscribe((pokemon) => {
            if (this.searchPokemonService.testPokemon(pokemon.id, this.searchWords)){
              this.pokemons.push(pokemon);
            }
          });
        }
      }
    }
  }*/

  public onScroll(): void {
    this.page++;
    // this.loadMoreResults();
  }

}

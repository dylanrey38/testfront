import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {CookieService} from 'ngx-cookie-service';
@Injectable({
  providedIn: 'root'
})
export class FileUploadService {

// API url
  baseApiUrl = 'http://127.0.0.1:8080/api/users/picture_upload';

  constructor(private http: HttpClient, private cookieService: CookieService) { }

// Returns an observable
  upload(file: any): Observable<any> {

    // Create form data
    const formData = new FormData();

    // Store form name as "file" with file data
    formData.append('docfile', file, file.name);
    formData.append('userID', this.cookieService.get('UserID'));

    // Make http post request over api
    // with formData as req
    return this.http.post(this.baseApiUrl, formData);
  }
}

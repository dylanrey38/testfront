import { Language } from './pokeapi/pokapi';

export interface Lang{
  label: string;
  langCode: string;
}

export interface Pokemon {
  id: number;
  names: Lang[];
  weight: number; // Kg
  height: number; // m
  type: string;
  type2?: string;
  typeNames: Lang[];
  type2Names?: Lang[];
  iconSrc: string;
  artworkSrc: string;
  stats: PokemonStats;
  areas: {
    id: number,
    name: string
  }[];
}

export interface PokemonStats {
  hp: number;
  attack: number;
  defense: number;
  specialAttack: number;
  specialDefense: number;
  speed: number;
}

import { Lang, Pokemon } from './pokemon';

export interface AreaListItem {
  id: number;
  name: string;
  location: {names: Lang[], id: number};
}

export interface AreaDetail extends AreaListItem {
  pokemons: Pokemon[];
  location: Location;
  names: Lang[];
}

export interface Location {
  id: number;
  names: Lang[];
  region: string;
  areas: AreaDetail[];
}

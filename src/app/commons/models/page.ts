export interface Page<T> {
  count: number;
  content: T[];
}

import { Pipe, PipeTransform } from '@angular/core';
import { Lang } from '../models/pokemon';
import { PokeTranslateService } from '../../poke-translate.service';

@Pipe({
  name: 'pokeTranslate',
  pure: false
})
export class PokeTranslatePipe implements PipeTransform {

  lastVal: Lang[] = [];
  lastLang: string;
  lastRes = '';

  constructor(public readonly pokeTranslateService: PokeTranslateService) {
    this.lastLang = pokeTranslateService.currentPokeLangue.value;
  }

  transform(value?: Lang[], or: string = ''): string {
    if (!value?.length) {
      return or;
    }
    const lang = this.pokeTranslateService.currentPokeLangue.value;
    if (value === this.lastVal && this.lastLang === lang) {
      return this.lastRes;
    }
    this.lastVal = value;
    this.lastLang = lang;
    const langFound = value.find(l => l.langCode === lang) || value.find(l => l.langCode === 'en') || value[0];
    return this.lastRes = langFound.label || or;


  }

}

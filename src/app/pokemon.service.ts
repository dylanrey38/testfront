import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Page } from './commons/models/page';
import { Lang, Pokemon } from './commons/models/pokemon';
import {
  APIResourceList,
  LocationAreaEncounter,
  NamedAPIResource,
  PokemonBase,
  PokemonSpecies,
  Type
} from './commons/models/pokeapi/pokapi';

export interface PokemonApiDatas{
  base: PokemonBase;
  areasEncounter: LocationAreaEncounter[];
  species: PokemonSpecies | null;
  types: Lang[][];
}

@Injectable({
  providedIn: 'root'
})
export class PokemonService {

  private readonly typeRequest: Record<string, Promise<Type>> = {};

  constructor(private http: HttpClient) {
  }

  public async pokemonUrlToPokemon(url: string): Promise<Pokemon | null> {
    return await this.getPokemonById(this.parsePokemonUrl(url));
  }

  public async getTypeLang(typeName: string): Promise<Lang[]> {
    if (!this.typeRequest[typeName]) {
      this.typeRequest[typeName] = this.http.get<Type>('https://pokeapi.co/api/v2/type/' + typeName).toPromise();
    }
    const type = await this.typeRequest[typeName];
    return type.names.map((name) => ({label: name.name, langCode: name.language.name}));
  }

  public parsePokemonUrl(url: string): number {
    const res = /pokemon\/([0-9]+)/.exec(url);
    if (!res) {
      throw new Error('fail tu parse pokemon URL ' + url);
    }
    return parseInt(res[1], 10);
  }

  public pokemonApiToPokemon(data: PokemonApiDatas): Pokemon {
    return {
      id: data.base.id,
      names: data.species
        ? data.species.names.map((name) => ({label: name.name, langCode: name.language.name}))
        : [{label: data.base.name, langCode: 'en'}],
      type: data.base.types[0].type.name,
      type2: data.base.types[1]?.type.name,
      typeNames: data.types[0],
      type2Names: data.types[1],
      iconSrc: data.base.sprites.front_default || data.base.sprites.other['official-artwork'].front_default,
      artworkSrc: data.base.sprites.other['official-artwork'].front_default,
      weight: data.base.weight / 10, // hectograms to Kg
      height: data.base.height / 10, // decimetre to metre
      areas: data.areasEncounter.map(this.mapLocationAreaEncounter),
      stats: {
        hp: data.base.stats.find((stat) => stat.stat.name === 'hp')?.base_stat || 0,
        attack: data.base.stats.find((stat) => stat.stat.name === 'attack')?.base_stat || 0,
        defense: data.base.stats.find((stat) => stat.stat.name === 'defense')?.base_stat || 0,
        specialAttack: data.base.stats.find((stat) => stat.stat.name === 'special-attack')?.base_stat || 0,
        specialDefense: data.base.stats.find((stat) => stat.stat.name === 'special-defense')?.base_stat || 0,
        speed: data.base.stats.find((stat) => stat.stat.name === 'speed')?.base_stat || 0,
      }
    };
  }

  private mapLocationAreaEncounter(encounter: LocationAreaEncounter): { id: number, name: string } {
    const urlParseMatch = /location\-area\/([0-9]+)/.exec(encounter.location_area.url) as RegExpExecArray;
    return {
      name: encounter.location_area.name,
      id: parseInt(urlParseMatch[1], 10)
    };
  }

  public async getPokemons(page: number = 0, pageSize: number = 20): Promise<Page<Pokemon>> {
    const resRequest = await this.http.get<APIResourceList<NamedAPIResource<PokemonBase>>>(`https://pokeapi.co/api/v2/pokemon?limit=${pageSize}&offset=${page * pageSize}`).toPromise();
    const pokemonRefs = resRequest.results;
    const pokemonsPromise: Promise<Pokemon | null>[] = [];

    for (const pokemonRef of pokemonRefs) {
      const pokemon = this.pokemonUrlToPokemon(pokemonRef.url);
      pokemonsPromise.push(pokemon);
    }
    const pokemons = await Promise.all(pokemonsPromise);
    return {count: resRequest.count, content: pokemons.filter((value) => value) as Pokemon[]};
  }

  public async getPokemonApiDatasById(id: number): Promise<PokemonApiDatas> {
    const url = `https://pokeapi.co/api/v2/pokemon/${id}`;
    const urlEncounters = `https://pokeapi.co/api/v2/pokemon/${id}/encounters`;
    const urlSpecies = `https://pokeapi.co/api/v2/pokemon-species/${id}/`;
    const [base, areasEncounter, species] = await Promise.all([
      this.http.get<PokemonBase>(url).toPromise(),
      this.http.get<LocationAreaEncounter[]>(urlEncounters).toPromise().catch((e) => []),
      this.http.get<PokemonSpecies>(urlSpecies).toPromise().catch((e) => null)
    ]);
    return  {
      base,
      areasEncounter,
      species,
      types: await Promise.all(base.types.map(t => this.getTypeLang(t.type.name)))
    };
  }

  public async getPokemonById(id: number): Promise<Pokemon | null> {
    try {
      const pokemonApiData = await this.getPokemonApiDatasById(id);
      return this.pokemonApiToPokemon(pokemonApiData);
    } catch (e) {
      return null;
    }
  }
}

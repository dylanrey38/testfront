import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import {CookieService} from 'ngx-cookie-service';

@Component({
  selector: 'app-game-carousel',
  templateUrl: './game-carousel.component.html',
  styleUrls: ['./game-carousel.component.scss']
})

export class GameCarouselComponent implements OnInit {

  private url = 'http://127.0.0.1:8080/api/games/best_preference_games/';
  @Input() customTitle!: string;

  show = true;

  private userId = 0;

  public images = [
  ];

  constructor(private router: Router,
              private http: HttpClient,
              private cookieService: CookieService) { }

  ngOnInit(): void {
    console.log(this.customTitle);
    this.userId = parseInt(this.cookieService.get('UserID'), 10);
    if (this.customTitle === 'Selected for you') {
      this.url = 'http://127.0.0.1:8080/api/games/best_preference_games/' + this.userId;
    }
    else if (this.customTitle === 'New released games') {
      this.url = 'http://127.0.0.1:8080/api/games/best_new_games/' + this.userId;
    }
    else if (this.customTitle === 'Best rated games') {
      this.url = 'http://127.0.0.1:8080/api/games/best_rated_games/' + this.userId;
    }
    else if (this.customTitle === 'Other players also like') {
      this.url = 'http://127.0.0.1:8080/api/games/other_liked_games/' + this.userId;
    }
    else {
      this.url += this.userId;
    }


    this.http.get<any>(this.url)
      .subscribe({
        next: data => {
          if (data.statut === -1) {
            this.show = false;
          }
          else {
            for (let i = 0; i < data.length; i++) {
              // @ts-ignore
              this.images.push( { path: data[i][2].cover_url, id: data[i][0].id} );
            }
          }
        },
        error: error => {
          this.show = false;
        }
      });
  }

  handleCarouselEvents($event: any): void {
    if ($event.name === 'click') {
      this.router.navigateByUrl('game/' + $event.image.id);
    }
  }
}

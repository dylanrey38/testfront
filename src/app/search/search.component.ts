import {Component, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {PokemonService} from '../pokemon.service';
import {SearchPokemonService} from '../search-pokemon.service';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  public games: any[] = [];
  public searchWords: string[] = [];
  public count = 0;
  public page = 0;
  public nextSearchIndex = 0;

  private searchWordsSubscription?: Subscription;
  private watchIndexedPokemonsSubscription?: Subscription;

  searchText: any = '';
  public images = [
  ];


  constructor(
    private pokemonService: PokemonService,
    private searchPokemonService: SearchPokemonService,
    private router: Router,
    private http: HttpClient,
  ) {
  }

  ngOnInit(): void {
    this.page = 0;
    this.nextSearchIndex = 0;
    this.games = [];

    this.http.post<any>(
      'http://127.0.0.1:8080/api/games/research',
      '{"text":"' + this.searchText + '", "page":"' + this.page + '"}')
      .subscribe({
        next: data => {
          this.games = [];
          for (const game of data) {
            this.games.push(game);
          }
        }
      });
  }

  onSearchChange(event: string): void {
    this.page = 0;
    this.http.post<any>(
      'http://127.0.0.1:8080/api/games/research',
      '{"text":"' + this.searchText + '", "page":"' + this.page + '"}')
      .subscribe({
      next: data => {
        this.games = [];
        for (const game of data) {
          this.games.push(game);
        }
      }
    });
  }

  private loadMoreResults(): void {
    this.http.post<any>(
      'http://127.0.0.1:8080/api/games/research',
      '{"text":"' + this.searchText + '", "page":"' + this.page + '"}')
      .subscribe({
        next: data => {
          for (const game of data) {
            this.games.push(game);
          }
        }
      });
  }

  public onScroll(): void {
    this.page++;
    this.loadMoreResults();
  }

}



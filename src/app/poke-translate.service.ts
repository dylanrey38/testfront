import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { last } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PokeTranslateService {

  public currentPokeLangue: BehaviorSubject<string>;

  constructor() {
    const lang = localStorage.getItem('pokeLang') || 'fr';
    this.currentPokeLangue = new BehaviorSubject<string>(lang);
  }

  setlangue(lang: string): void{
    this.currentPokeLangue.next(lang);
    localStorage.setItem('pokeLang', lang);
  }
}

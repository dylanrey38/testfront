import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {ActivatedRoute} from '@angular/router';
import { Subscription, zip } from 'rxjs';
import {CookieService} from 'ngx-cookie-service';

@Component({
  selector: 'app-game-detail',
  templateUrl: './game-detail.component.html',
  styleUrls: ['./game-detail.component.scss']
})
export class GameDetailComponent implements OnInit {

  private subscription?: Subscription;

  public gameTitle = '';
  public description = '';
  public rating = '';
  public categoryList = new Array();
  public releaseDateList = Array();
  public platformList = [];
  public websiteLink = '';
  public studioList = [];
  public urlCover = '';
  public images = [];
  public bgImageLink = '';

  constructor(private http: HttpClient, private route: ActivatedRoute, private cookieService: CookieService) { }

  userId = -1;
  public colorLike = 'accent';
  public colorAdd = '#b0bec5';


  ngOnInit(): void {
    this.userId = parseInt(this.cookieService.get('UserID'), 10);
    window.scrollTo(0, 0);
    this.subscription = this.route.paramMap.subscribe((param) => {
      this.http.get<any>('http://127.0.0.1:8080/api/games/game_info/' + param.get('id') as string)
        .subscribe({
          next: data => {
            console.log(data);
            // Game infos
            this.gameTitle = data[0].name;
            if (data[0].summary === '-1') {
              this.description = 'No description';
            }
            else {
              this.description = data[0].summary;
            }
            if (data[0].aggregated_rating === -1) {
              this.rating = '-';
            }
            else {
              this.rating = String(data[0].aggregated_rating).substr(0, 2);
            }

            // Game gender
            for (const cat of data[1]) {
              // @ts-ignore
              this.categoryList.push(cat.name);
            }

            // Release date
            for (const date of data[3]) {
              // @ts-ignore
              const dateInfo: any[] = [];
              dateInfo.push(date.date);
              dateInfo.push(date.region);
              this.releaseDateList.push(dateInfo);
            }
            let i = 0;
            while (i < this.releaseDateList.length){
              let j = 0;
              while (j < i){
                if (this.releaseDateList[j][1].toString() == this.releaseDateList[i][1].toString()){
                  this.releaseDateList.splice(i, 1);
                  i--;
                }
                j++;
              }
              i++;
            }

            // Platforms
            for (const platform of data[7]) {
              // @ts-ignore
              this.platformList.push(platform.name);
            }

            // Website
            if (data[8].length > 0) {
              this.websiteLink = data[8][0].url;
            }

            // Game studio
            for (const studio of data[9]) {
              // @ts-ignore
              this.studioList.push(studio.name);
            }

            let k = 0;
            while (k < this.studioList.length){
              let j = 0;
              while (j < k){
                if (this.studioList[j] == this.studioList[k]){
                  this.studioList.splice(k, 1);
                  k--;
                }
                j++;
              }
              k++;
            }

            // Cover
            this.urlCover = data[10].cover_slug;

            // Screenshots
            for (const screenshot of data[11]) {
              // @ts-ignore
              this.images.push({path: screenshot.screenshot_slug});
            }
            this.bgImageLink = data[11][0].screenshot_slug;
          },
          error: error => { }
        });
    });

    this.subscription = this.route.paramMap.subscribe((param) => {
      this.http.get<any>('http://127.0.0.1:8080/api/users/doUserLikeGame/' + this.userId + '/' + param.get('id') as string)
        .subscribe({
          next: data => {
            if (data) {
              this.colorLike = 'warn';
            }
          }
        });
    });
    this.subscription = this.route.paramMap.subscribe((param) => {
      this.http.get<any>('http://127.0.0.1:8080/api/users/doUserAddGame/' + this.userId + '/' + param.get('id') as string)
        .subscribe({
          next: data => {
            if (data) {
              this.colorAdd = '#0ECE00';
            }
          }
        });
    });
  }

  ReadMore(): void {
    const element = document.querySelector('#desc');
    const word = document.getElementById('read-more');
    if (element !== null) {
      if (!element.classList.contains('full')) {
        element.classList.add('full');
        if (word !== null) {
          word.textContent = 'Read less';
        }
      }
      else {
        element.classList.remove('full');
        if (word !== null) {
          word.textContent = 'Read more';
        }
      }
    }
  }

  likeGame(): void {
    if (this.colorLike === 'accent') {
      this.colorLike = 'warn';
    }
    else {
      this.colorLike = 'accent';
    }
    this.subscription = this.route.paramMap.subscribe((param) => {
      this.http.get<any>('http://127.0.0.1:8080/api/games/userlike/' + this.userId + '/' + param.get('id') as string)
        .subscribe({
          next: data => {}
        });
    });
  }

  addGame(): void {
    this.subscription = this.route.paramMap.subscribe((param) => {
      this.http.get<any>('http://127.0.0.1:8080/api/games/useraddlist/' + this.userId + '/' + param.get('id') as string)
        .subscribe({
          next: data => {
            if (this.colorAdd === '#b0bec5') {
              this.colorAdd = '#0ECE00';
            }
            else {
              this.colorAdd = '#b0bec5';
            }
          }
        });
    });
  }
}

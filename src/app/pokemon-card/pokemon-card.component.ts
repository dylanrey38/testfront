import { Component, Input, OnInit } from '@angular/core';
import { Pokemon } from '../commons/models/pokemon';

@Component({
  selector: 'app-pokemon-card',
  templateUrl: './pokemon-card.component.html',
  styleUrls: ['./pokemon-card.component.scss']
})
export class PokemonCardComponent implements OnInit {

  @Input()
  public displayDetailButton = true;

  @Input('pokemon')
  public pokemon?: Pokemon;

  constructor() { }

  ngOnInit(): void {
  }
}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { isEqual } from 'lodash-es';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { APIResourceList, LocationAreaEncounter, NamedAPIResource, PokemonBase, PokemonSpecies } from './commons/models/pokeapi/pokapi';
import { Pokemon } from './commons/models/pokemon';
import { PokemonApiDatas, PokemonService } from './pokemon.service';

@Injectable({
  providedIn: 'root'
})
export class SearchPokemonService {

  private pokemonsWordsById: Record<number, string[]> = {};
  private pokemonsById: Record<number, Pokemon> = {};
  private searchWordsSubject: BehaviorSubject<string[]> = new BehaviorSubject<string[]>([]);
  private pokemonSearchId: number[] = [];
  private newPokemonIndexedSubject: Subject<Pokemon> = new Subject<Pokemon>();

  constructor(private http: HttpClient, private pokemonService: PokemonService) {
  }

  public getSearchWords(): Observable<string[]> {
    return this.searchWordsSubject;
  }

  public setSearch(search: string): void {
    const searchWords = this.getWords(search);

    if (!isEqual(this.searchWordsSubject.value, searchWords)) {
      this.searchWordsSubject.next(searchWords);
    }
  }

  public watchIndexedPokemeon(): Observable<Pokemon> {
    return this.newPokemonIndexedSubject;
  }

  public searchPokemon(words: string[], nbPokemon: number, startPokemonIndex: number = 0): [Pokemon[], number] {
    const res: Pokemon[] = [];

    for (let index = startPokemonIndex; index < this.pokemonSearchId.length; index++) {
      const pokemonId = this.pokemonSearchId[index];
      if (this.testPokemon(pokemonId, words)) {
        res.push(this.pokemonsById[pokemonId]);
        if (res.length >= nbPokemon) {
          return [res, index + 1];
        }
      }
    }
    return [res, -1];
  }

  private getWords(str: string): string[] {
    str = str.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
    str = str.toLowerCase();
    return str.match(/(\w+)/g) || [];
  }

  public async fillPokemonsWords(): Promise<void> {
    const resRequest = await this.http
      .get<APIResourceList<NamedAPIResource<PokemonBase>>>(`https://pokeapi.co/api/v2/pokemon?limit=2000`).toPromise();
    const intervalId = setInterval(() => (async () => {
      const pokeapiRef = resRequest.results.shift();
      if (!pokeapiRef) {
        clearInterval(intervalId);
        return;
      }
      try {
        const id = this.pokemonService.parsePokemonUrl(pokeapiRef.url);
        const pokeapiDatas = await this.pokemonService.getPokemonApiDatasById(id);
        const words = this.getPokemonWords(pokeapiDatas);
        const pokemon = this.pokemonService.pokemonApiToPokemon(pokeapiDatas);
        this.pokemonsWordsById[id] = words;
        this.pokemonSearchId.push(id);
        this.pokemonsById[id] = pokemon;
        this.newPokemonIndexedSubject.next(pokemon);
      } catch (e) {
        console.log('Pokemon not found ' + pokeapiRef.name);
      }
    })(), 25);
  }

  private getPokemonWords(data: PokemonApiDatas): string[] {
    const res: string[] = [];
    if (data.species) {
      const allNames = data.species.names.map((name) => name.name);
      for (const name of allNames) {
        res.push(...this.getWords(name));
      }
    }
    for (const type of data.types) {
      for (const typeName of type) {
        res.push(...this.getWords(typeName.label));
      }
    }
    res.push(...this.getWords(data.base.name));
    res.push(...data.base.types.map((type) => type.type.name));
    res.push(data.base.id.toString());
    return res;
  }

  testPokemon(pokemonId: number, searchWords: string[]): boolean {
    const pokemonWords = this.pokemonsWordsById[pokemonId];
    return (searchWords.every((searchWord) => pokemonWords.some((pokemonWord) => pokemonWord.startsWith(searchWord))));
  }
}

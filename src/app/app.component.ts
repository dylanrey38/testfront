import { Component, OnInit } from '@angular/core';
import { SearchPokemonService } from './search-pokemon.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {
  constructor(private searchPokemonService: SearchPokemonService) {}

  ngOnInit() {
    // setTimeout(() => this.searchPokemonService.fillPokemonsWords(), 2000);
  }
}

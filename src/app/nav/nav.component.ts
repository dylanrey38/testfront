import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatAutocompleteTrigger } from '@angular/material/autocomplete';
import { Router } from '@angular/router';
import { Pokemon } from '../commons/models/pokemon';
import { PokemonService } from '../pokemon.service';
import { SearchPokemonService } from '../search-pokemon.service';
import { TranslateService } from '@ngx-translate/core';
import { PokeTranslateService } from '../poke-translate.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  public pokemons?: Pokemon[];
  public formControl = new FormControl();
  @ViewChild(MatAutocompleteTrigger)
  public myAutoCompleteRef?: MatAutocompleteTrigger;
  @ViewChild('searchInput')
  public myInputRef?: ElementRef;
  siteLang: string;
  pokeLang: string;

  constructor(
    private searchPokemonService: SearchPokemonService,
    private pokemonService: PokemonService,
    private router: Router,
    private translateService: TranslateService,
    private pokeTranslateService: PokeTranslateService
  ) {
    this.siteLang = localStorage.getItem('i18n') || 'fr';
    translateService.use(this.siteLang);
    this.pokeLang = this.pokeTranslateService.currentPokeLangue.value;
  }

  ngOnInit(): void {
    this.pokemonService.getPokemons().then((page) => { this.pokemons?.push(...page.content); });
    this.formControl.valueChanges.subscribe((value) => {
      if (typeof value === 'string') {
        this.searchPokemonService.setSearch(value);
      }
    });
    this.searchPokemonService.getSearchWords().subscribe((words) => {
      this.pokemons = this.searchPokemonService.searchPokemon(words, 10)[0];
    });
  }

  public onClickPokemon(pokemon: Pokemon): void {
    this.formControl.setValue('');
    this.router.navigate(['detail', pokemon.id]);
    this.myAutoCompleteRef?.closePanel();
    this.myInputRef?.nativeElement.blur();
  }

  public submitForm(): void {
    if (this.pokemons?.length === 1) {
      this.onClickPokemon(this.pokemons[0]);
    } else {
      this.router.navigate(['home']);
      this.myAutoCompleteRef?.closePanel();
      this.myInputRef?.nativeElement.blur();
    }
  }

  setSiteLang(langue: string): void {
    localStorage.setItem('i18n', langue);
    this.translateService.use(langue);
  }

  setPokeLang(pokeLang: string): void {
    this.pokeLang = pokeLang;
    this.pokeTranslateService.setlangue(pokeLang);
  }
}

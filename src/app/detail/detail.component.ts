import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription, zip } from 'rxjs';
import { Pokemon } from '../commons/models/pokemon';
import { PokemonService } from '../pokemon.service';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Label } from 'ng2-charts';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit, OnDestroy {

  private subscription?: Subscription;
  public pokemon ?: Pokemon;


  public barChartOptions: ChartOptions = {
    responsive: true,
    scales: {
      yAxes: [{
         ticks: {
            min: 0,
            max: 250
         }
      }]
   }
  };

  public barChartLabels: Label[] = ['HP', 'Attack', 'Defense', 'Special Attack', 'Special Defense', 'Speed'];
  public barChartType: ChartType = 'bar';
  public barChartLegend = false;
  public barChartPlugins = [];

  public barChartData: ChartDataSets[] = [{
    data: [65, 59, 80, 81, 56, 55],
    backgroundColor: [
      'rgba(255, 99, 132, 0.2)',
      'rgba(54, 162, 235, 0.2)',
      'rgba(255, 206, 86, 0.2)',
      'rgba(75, 192, 192, 0.2)',
      'rgba(153, 102, 255, 0.2)',
      'rgba(255, 159, 64, 0.2)'
    ],
    borderColor: [
      'rgba(255, 99, 132, 1)',
      'rgba(54, 162, 235, 1)',
      'rgba(255, 206, 86, 1)',
      'rgba(75, 192, 192, 1)',
      'rgba(153, 102, 255, 1)',
      'rgba(255, 159, 64, 1)'
    ],
    hoverBackgroundColor: [
      'rgba(255, 99, 132, 1)',
      'rgba(54, 162, 235, 1)',
      'rgba(255, 206, 86, 1)',
      'rgba(75, 192, 192, 1)',
      'rgba(153, 102, 255, 1)',
      'rgba(255, 159, 64, 1)'
    ],
    hoverBorderWidth: 0,
    borderWidth: 1
  }];

  constructor(
    private route: ActivatedRoute,
    private pokemonService: PokemonService
  ){}

  ngOnInit(): void {
    this.subscription = this.route.paramMap.subscribe((param) => {
      this.pokemonService.getPokemonById(parseInt(param.get('id') as string)).then((pokemon) => {
        this.pokemon = pokemon || undefined;
        const stats = this.pokemon?.stats;
        this.barChartData[0].data = [ stats?.hp, stats?.attack, stats?.defense, stats?.specialAttack, stats?.specialDefense, stats?.speed ];
      });
    });
  }

  ngOnDestroy(): void {
    this.subscription?.unsubscribe();
  }
}

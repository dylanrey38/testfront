import { Component, OnInit } from '@angular/core';
import { LocationService } from './location.service';
import { Location } from '../commons/models/area';
import { PageEvent } from '@angular/material/paginator';

@Component({
  selector: 'app-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.scss']
})
export class LocationComponent implements OnInit {
  locations: Location[] = [];
  page = 0;
  pageSize = 20;
  total = 20;
  displayedColumns: string[] = ['region', 'name', 'areas'];

  constructor(private readonly locationService: LocationService) {
  }

  async ngOnInit(): Promise<void> {
    this.loadPage(0);
  }

  async loadPage(index: number): Promise<void> {
    this.locations = [];
    const {count, content} = await this.locationService.getPage(index, this.pageSize);
    this.locations = content;
    this.total = count;
    this.page = index;
  }

  pageChange(pageEvent: PageEvent): void {
    this.pageSize = pageEvent.pageSize;
    this.loadPage(pageEvent.pageIndex);
  }
}

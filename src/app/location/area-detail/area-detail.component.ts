import { Component, OnDestroy, OnInit } from '@angular/core';
import { AreaDetail } from '../../commons/models/area';
import { LocationService } from '../location.service';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-area-detail',
  templateUrl: './area-detail.component.html',
  styleUrls: ['./area-detail.component.scss']
})
export class AreaDetailComponent implements OnInit, OnDestroy {
  area?: AreaDetail;
  paramSub?: Subscription;
  loading: boolean = true;

  constructor(
    private readonly locationService: LocationService,
    private readonly activeRoute: ActivatedRoute,
    ) { }

  ngOnInit(): void {
    this.paramSub = this.activeRoute.params.subscribe(async (params) =>  {
      this.loading = true;
      this.area = await this.locationService.getArea(params.id);
      this.loading = false;
    });
  }

  ngOnDestroy(): void {
    this.paramSub?.unsubscribe();
  }

}

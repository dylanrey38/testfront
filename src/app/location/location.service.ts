import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {
  APIResourceList,
  Location as LocationAPI,
  LocationArea,
  NamedAPIResource,
  PokemonEncounter
} from '../commons/models/pokeapi/pokapi';
import { AreaDetail, AreaListItem, Location } from '../commons/models/area';
import { Page } from '../commons/models/page';
import { Pokemon } from '../commons/models/pokemon';
import { PokemonService } from '../pokemon.service';

@Injectable({
  providedIn: 'root'
})
export class LocationService {

  constructor(
    private readonly http: HttpClient,
    private readonly pokemonService: PokemonService
  ) {
  }

  async getPage(page: number, pageSize: number): Promise<Page<Location>> {
    const pageAPI = await this.http.get<APIResourceList<NamedAPIResource<LocationAPI>>>(
      'https://pokeapi.co/api/v2/location/',
      {params: {offset: (pageSize * page).toString(), limit: pageSize.toString()}}).toPromise();
    const mapRes = pageAPI.results.map(async (locationRef) => {
      return await this.getLocation(locationRef.url);
    });
    const res = await Promise.all(mapRes);
    return {
      count: pageAPI.count,
      content: res.filter((l) => l) as Location[]
    };
  }

  async getLocation(locationUrl: string): Promise<Location | null> {
    try {
      const location = await this.http.get<LocationAPI>(locationUrl).toPromise();
      return {
        id: location.id,
        names: location.names.map((lang) => ({ label: lang.name, langCode: lang.language.name})),
        region: location.region.name,
        areas: await this.getLocationArea(location)
      };
    } catch (e) {
      return null;
    }
  }

  async getLocationArea(location: LocationAPI): Promise<AreaDetail[]> {
    const mesPromises = location.areas.map(async (areaRef) => {
      try {
        const areaApi = await this.http.get<LocationArea>(areaRef.url).toPromise();
        return {
          id: areaApi.id,
          name: areaApi.name,
          names: areaApi.names.map((lang) => ({ label: lang.name, langCode: lang.language.name})),
          location: {
            id: location.id,
            names: location.names.map((lang) => ({ label: lang.name, langCode: lang.language.name})),
          }
        } as AreaDetail;
      } catch (e) {
        return null;
      }
    });
    const res = await Promise.all(mesPromises);
    return res.filter((area) => area) as AreaDetail[];
  }

  async getArea(id: number): Promise<AreaDetail> {
    const areaAPI = await this.http.get<LocationArea>('https://pokeapi.co/api/v2/location-area/' + id.toString()).toPromise();
    const [pokemons, location] = await Promise.all([
      this.getPokemonsEncounter(areaAPI.pokemon_encounters),
      this.getLocation(areaAPI.location.url) as Promise<Location>
    ]); // parallelise getPokemonsEncounter and getLocation

    return {
      id: areaAPI.id,
      name: areaAPI.name,
      names: areaAPI.names.map((lang) => ({ label: lang.name, langCode: lang.language.name})),
      pokemons,
      location
    };
  }

  private async getPokemonsEncounter(pokemonsEncounter: PokemonEncounter[]): Promise<Pokemon[]> {
    const res: Promise<Pokemon | null>[] = pokemonsEncounter.map((pokemonEncounter) => {
      return this.pokemonService.pokemonUrlToPokemon(pokemonEncounter.pokemon.url);
    });
    const res2 = await Promise.all(res);
    return res2.filter((pokemon) => pokemon) as Pokemon[];
  }

}

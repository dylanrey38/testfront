import { TestBed } from '@angular/core/testing';

import { PokeTranslateService } from './poke-translate.service';

describe('PokeTranslateService', () => {
  let service: PokeTranslateService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PokeTranslateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
